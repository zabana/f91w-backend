import os


class Config(object):
    """
        Main configuration object for f91w.
    """

    S3_BUCKET = os.environ.get("S3_BUCKET")
    S3_KEY = os.environ.get("S3_KEY")
    S3_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_ACCESS_KEY")


class ProductionConfig(Config):
    """
        Production settings for f91w.
    """
    ENV = 'production'
    DEBUG = False


class DevelopmentConfig(Config):
    """
        Development settings for f91w.
    """
    ENV = 'development'
    DEBUG = True
    TESTING = False
