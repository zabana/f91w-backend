FROM python:3.6

MAINTAINER Karim Cheurfi <karim.cheurfi@gmail.com>

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

COPY . /app

COPY ./docker-entrypoint.sh /usr/bin/docker-entrypoint.sh

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]

# CMD ["wsgi.py"]
