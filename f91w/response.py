from flask import make_response, jsonify


class Response():

    """
    Response Class. Contains methods that will return
    JSON responses to the client
    """

    @classmethod
    def ok_response(cls, body=None):
        """HTTP OK Response (Status Code: 200)"""
        response = {
            "meta": {"status": 200, "message": "OK"},
            "body": body
        }
        return make_response(jsonify(response), 200)

    @classmethod
    def bad_request(cls, message=None):
        """HTTP Bad Request Response (Status Code: 400)"""
        response = {
            "meta": {"status": 400, "error": "Bad request"},
            "body": {
                "message": message
            }
        }
        return make_response(jsonify(response), 400)

    def server_error(self):
        """HTTP Server Error Response (Status Code: 500)"""
        pass
