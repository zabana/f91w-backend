ALLOWED_EXTENSIONS = set(['jpg', 'jpeg', 'gif'])


def allowed_file(name):
    """Checks if the file is in the correct format"""
    return "." in name and name.split(".")[1].lower() in ALLOWED_EXTENSIONS
