import boto3
from flask import Flask, render_template, request
from flask_cors import CORS
from f91w.service import F91WService
import os

CONFIG = {
    "prod": "config.ProductionConfig",
    "staging": "config.StagingConfig",  # useless but hey
    "default": "config.DevelopmentConfig"
}

app = Flask(__name__)
config_name = os.environ.get('F91W_BACKEND_CONFIG', 'default')
app.config.from_object(CONFIG[config_name])
CORS(app)

S3 = boto3.client(
    's3',
    aws_access_key_id=app.config['S3_KEY'],
    aws_secret_access_key=app.config['S3_SECRET_ACCESS_KEY']
)

S3_RESOURCE = boto3.resource(
    's3',
    aws_access_key_id=app.config['S3_KEY'],
    aws_secret_access_key=app.config['S3_SECRET_ACCESS_KEY']
)


@app.route("/upload", methods=['GET'])
def upload_form():
    "Upload form view"
    return render_template('upload_form.html')


@app.route("/photos")
def get_all_photos():
    "Retrieve all filenames in the bucket"
    f91w_bucket = S3_RESOURCE.Bucket(app.config['S3_BUCKET'])
    return F91WService.list_all_photos(f91w_bucket)


@app.route("/photos", methods=['POST'])
def upload_photo():
    "Upload photo controller"
    f91w_bucket = S3_RESOURCE.Bucket(app.config['S3_BUCKET'])
    return F91WService.upload_photo(request, f91w_bucket)
