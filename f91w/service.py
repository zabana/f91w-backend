from werkzeug.utils import secure_filename
from f91w.response import Response
from f91w.helpers import allowed_file


class F91WService():

    """
    Service Class.
    This where all the business logic is handled.
    """

    @classmethod
    def list_all_photos(cls, bucket):
        """Will retrieve all filename for the pictures stored in the bucket"""
        photos = [
            {
                'name': photo.key.title().lower(),
                'lastModified': photo.last_modified
            }
            for photo in bucket.objects.all()
        ]
        response_body = {"photos": photos}
        return Response.ok_response(body=response_body)

    @classmethod
    def upload_photo(cls, request, bucket):
        """
        Ensures that there is a file and that it conforms to the expected
        format
        Then uploads it to the s3 bucket.
        """

        if 'photo' not in request.files:
            error_msg = "No multipart file. Expected a file input called photo"
            return Response.bad_request(message=error_msg)

        file = request.files['photo']

        if file.filename == "":
            error_msg = "Please select a file to send"
            return Response.bad_request(message=error_msg)

        if not file and not allowed_file(file.filename):
            error_msg = "File type not allowed. Please choose from one of the \
            following formats: jpg, jpeg, gif"
            return Response.bad_request(message=error_msg)

        filename = secure_filename(file.filename)
        # do: Add a try except and see what's up
        bucket.Object(filename).put(Body=file)
        response_body = {"message": "File uploaded successfully."}
        return Response.ok_response(body=response_body)
