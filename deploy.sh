  #!/bin/sh

DESTINATION_PATH="/home/$DEPLOY_USER/projects/f91w-backend"

if [ "$DEPLOY_CONTEXT" = 'staging' ]; then
    SERVER_IP=$STAGING_IP
elif [ "$DEPLOY_CONTEXT" = 'production' ]; then
    SERVER_IP=$PRODUCTION_IP
fi

# 1. stop all containers with f91w-api as their name
ssh -o StrictHostKeyChecking=no $DEPLOY_USER@$SERVER_IP 'docker stop $(docker ps -aq --filter name=f91w-api)'

# 2. remove all containers and dangling images
ssh $DEPLOY_USER@$SERVER_IP 'docker container prune -f && docker rmi -f $(docker images -f "dangling=true" -q) || true'

echo "Copying saved image to destination directory located at: $DESTINATION_PATH"
scp -o StrictHostKeyChecking=no -r f91w-backend $DEPLOY_USER@$SERVER_IP:$DESTINATION_PATH
echo "Done !"

echo "Accessing $DESTINATION_PATH and loading the image"
ssh $DEPLOY_USER@$SERVER_IP "cd /home/$DEPLOY_USER/projects/f91w-backend && docker load -i f91w-backend"
echo "Done !"

echo "Attempting to run the image by mapping port $RUNNING_PORT to the local
port inside the container"
ssh $DEPLOY_USER@$SERVER_IP "docker run -d -p $RUNNING_PORT:5000 \
        -e S3_BUCKET=$S3_BUCKET \
        -e S3_KEY=$S3_KEY \
        -e S3_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY \
        --name f91w-api f91w-backend:latest";

echo "Deployed to $DEPLOY_CONTEXT!"
