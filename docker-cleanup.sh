#!/bin/sh

# 1. stop all containers with f91w-backend as their name
ssh -o StrictHostKeyChecking=no $DEPLOY_USER@$STAGING_IP "docker stop $(docker ps -aq --filter name=f91w-api)"

# 2. remove all containers and dangling images
ssh $DEPLOY_USER@$STAGING_IP "docker container prune -f && docker rmi -f $(docker images -f "dangling=true" -q)"

